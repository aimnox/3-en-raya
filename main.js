var jugador = 0;
var boardStr = "";
var won = false;
var casillasLibres = 9;
var ptsX = 0;
var ptsO = 0;
const posibilities = [
    ["1", "4", "7"], //c1
    ["2", "5", "8"], //c2
    ["3", "6", "9"], //c3
    ["1", "2", "3"], //f1
    ["4", "5", "6"], //f2
    ["7", "8", "9"], //f3
    ["1", "5", "9"], //d1
    ["3", "5", "7"] //d2
];

function jugada(casella) {
    //Si la partida a acabat fes reset
    if (won) {
        reset();
        return;
    }

    //Si la casella esta ocupada, ignora
    if (casella.ocupada) {
        return;
    }

    //Ocupar casella
    casella.ocupada = true;
    casillasLibres--;

    if (jugador) {
        casella.innerText = "X";
    } else {
        casella.innerText = "O";
    }

    //Algun guanyador/empat?
    checkGameBoard();
    nextTurn();
}

function nextTurn() {
    //Jugador 0/1 -> <-
    jugador = !jugador;
    document.getElementById("turno").innerText = "Turno del Jugador " + (jugador + 1);

}

//Comproba guanyadors
function checkGameBoard() {
    buildBoardStr();

    //Encuentra 1 numero seguido de 3 caracteres iguales. Extrae el numero y el caracter

    var winner = boardStr.match(/(\d)(.)\2{2,}/);

    if (winner != null) { //Hay ganador
        win(winner);
    } else if (casillasLibres == 0) { //No quedan casillas dispoibles, empate
        alert("Empate");
        won = true;
    }

}

function buildBoardStr() {
    //Construye un string con todos los posibles grupos victoriosos
    boardStr = "";
    var j = 0;
    posibilities.forEach(posi => {
        boardStr += j++;
        posi.forEach(cassella => {
            boardStr += getText(cassella);
        })
    })
    console.log(boardStr);

}

function win(winInfo) {
    showWinRow(winInfo[1]); //Posa els guanyadors en verd
    updateScore(winInfo[2]); //Actualitza puntuacio
    winInfo[2] == "X" ? alert("Gano el Jugador 2") : alert("Gano el Jugador 1");
    won = true;
}

function showWinRow(num) {
    //Pinta la ratlla guanyadora de verd
    posibilities[num].forEach(id => {
        document.getElementById(id).style.color = "green";
    })
}

function updateScore(winner) {
    //Augmenta la puntuacio
    if (winner == "O") {
        document.getElementById("ptsX").innerText = "Jugador 1: " + ++ptsX;
    } else {
        document.getElementById("ptsO").innerText = "Jugador 2: " + ++ptsO;

    }
}

function reset() {
    //Borra buida i pinta en negre totes les casselles
    Array.from(document.getElementsByClassName("cell")).forEach(cell => {
        cell.innerText = "";
        cell.ocupada = false;
        cell.style.color = "black";
    });
    //Var reset
    jugador = 0;
    won = false;
    casillasLibres = 9;
}

function getText(id) {
    return getCell(id).innerText;
}

function getCell(id) {
    return document.getElementById(id);
}